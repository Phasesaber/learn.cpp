/*

A Parser written in C++.
There will be loads of errors, as this is 
the first thing I've ever made in C++ 
(probably not the smartest choice...).

The concept of it is cool though, 
so go modify it and make it work amazingly!

Licensed under the MIT License.

*/
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

void insertCode(){
	string bin = "n11 +001 n1";
	
	ofstream writer("add.bin");
	writer << bin;
	writer.close();
}

int main(){
	
	insertCode();
	
	ifstream reader("add.bin");
	
	char letter;
	string token;
	
	vector<string> codes(0);
	
	for(int i = 0; !reader.eof(); i++){
		reader.get(letter);
		if(letter == ' '){ 
			codes.push_back(token);
			token = "";
		}
		else{
			token += letter;
		}
	}
	
	reader.close();
	
	vector<int> nums(0);
	
	for(int i = 0; i < codes.size(); i++){
		string c = codes.get(i);
		if(startsWith(c, "n")){
			c.erase(0, 1);
			//TODO nums.insert();
		}
	}
	
}

bool startsWith(string s, string sub){
	return strncmp(s, sub, strlen(s) == 0;
}